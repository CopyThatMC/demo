import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    history: []
  },
  getters: {

    locallHistory: (state) => {

      if (localStorage.getItem("history")) {

        let list = JSON.parse(localStorage.getItem("history"))

        if (list.length > 10) {
          state.history = list.slice(0, 10)
        } else {
          state.history = list
        }
      }

      return state.history
    }


  },
  mutations: {

    SET_LOCALHISTORY (state, record) {

      let list = state.history
      let i = list.findIndex(item => item == record)
      if (i > -1) {
        list.splice(i, 1)
      }

      //重新存放数据
      if (list.length == 0) {
        list = JSON.stringify([record])
      } else {
        list = JSON.stringify([record, ...list])
      }
      // //存放到本地缓存
      localStorage.setItem("history", list)
      state.history = list
    }
  },
  actions: {
  },
  modules: {
  }
})
