import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import animated from 'animate.css' // css动画
import ElementUI from 'element-ui' // Element-Ui 组件
import 'element-ui/lib/theme-chalk/index.css' // Element-Ui 样式

// Vue.prototype.$echarts = window.echarts // echart
Vue.use(animated)
Vue.use(ElementUI, { size: 'mini', zIndex: 3000 })
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
