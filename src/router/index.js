import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/layout';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    name: '首页',
    hidden: true,
    children: [{
      path: 'index',
      component: () => import('@/views/index').then(m => m.default),
    }]
  },

  {
    path: '/base',
    component: Layout,
    name: '基础配置',
    hidden: true,
    children: [{
      path: '/base/enum',
      component: () => import('@/views/base/enum').then(m => m.default),
    }]
  },




]

const router = new VueRouter({
  routes
})

export default router
