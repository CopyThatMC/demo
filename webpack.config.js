const path = require('path')
const HappyPack = require('happypack') // 提高打包速度
const VueLoaderPlugin = require('vue-loader/lib/plugin') // vue-loader 插件 
const HtmlWebpackPlugin = require('html-webpack-plugin') // 简化 html 创建 ==>(https://github.com/jantimon/html-webpack-plugin)
const CopyWebpackPlugin = require('copy-webpack-plugin') // 将单个文件或整个目录复制到构建目录
const { CleanWebpackPlugin } = require('clean-webpack-plugin') // 清除打包文件夹下多余文件 详细配置==>(https://www.npmjs.com/package/clean-webpack-plugin)
const CompressionWebpackPlugin = require('compression-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin')     //打包的css拆分,将一部分抽离出来  

module.exports = {

  entry: {  // 文件入口
    app: ['babel-polyfill', path.join(__dirname, './src/main.js')]
  },

  mode: 'development', // 当前环境

  output: {  // 输出
    filename: 'bundle.js', //输出的文件名
    path: path.join(__dirname, './dist') //输出的路径
  },

  module: {       //模块的相关配置
    rules: [     //根据文件的后缀提供一个loader,解析规则 需要 npm install ?-loader
      { test: /\.vue$/, use: ['vue-loader'] }, // .vue转换
      { test: /\.css$/, use: ['style-loader', 'css-loader'] }, // .css 转换
      {
        test: /\.(jsx|js)$/,
        include: [path.resolve(__dirname, 'src')],
        loader: 'happypack/loader?id=happybabel',
        exclude: /node_modules/,
      }, // .js 转换
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        query: { limit: 9000, name: '[name].[ext]?[hash]' }
      }, // 图标格式转换
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        query: { limit: 5000, name: '[name].[ext]?[hash]' }
      }// 文件格式转换
    ]
  },

  resolve: { //解析模块的可选项  
    // modules: [ ]//模块的查找目录 配置其他的css等文件
    extensions: [".js", ".json", ".jsx", ".less", ".css", '.vue'],  //用到文件的扩展名
    alias: { //模快别名列表
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve(__dirname, './src'),
      '#': path.resolve(__dirname, './static')
    }
  },

  plugins: [  //插件的引用, 压缩，分离美化

    new VueLoaderPlugin(),
    new HappyPack({// 提高打包速度
      id: 'happybabel',
      loaders: ['babel-loader?cacheDirectory'],
    }),
    new CleanWebpackPlugin(),// 每次打包清除dist下的文件
    new HtmlWebpackPlugin({  //!3.2.0版本和optimization连用会导致打包js加载不上，使用4.0.0-beta.11版本解决
      filename: 'index.html',// 打包后生成的 html 文件名
      title: "webpack-主页", // html的title
      template: path.resolve(__dirname, 'src/index.html'), //使用的模板
      // inject: "head",// js放在哪里 一般不要放在head里面 
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, './static'),
        to: "./static",
        // ignore: ['.*']
      }
    ]),

    new CompressionWebpackPlugin({
      algorithm: 'gzip',
      test: /\.(js|css)$/,// 匹配文件名
      threshold: 10240,
      minRatio: 0.8,
      performance: {
        //入口起点的最大体积
        maxEntrypointSize: 50000000,
        //生成文件的最大体积
        maxAssetSize: 30000000,
      }
    }),

    // new ExtractTextPlugin('[name].css'),  //[name] 默认  也可以自定义name  声明使用

  ],



  externals: {
    'echarts': 'echarts'
  },

  devServer: {
    hitstoryApiFalllback: {
      index: '/dist/index.html'
    },
    host: '127.0.0.1',
    prot: '9000',
    hot: true,
    diableHostCheck: false,
  }
}