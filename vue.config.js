"use strict"

const path = require('path')

module.exports = {

  publicPath: process.env.NODE_ENV === 'production' ? "/" : "/",  // 部署应用包时的基本 URL

  lintOnSave: true, // 是否在开发环境下通过 eslint-loader 在每次保存时 lint 代码 设置为 true 或 'warning' 时，eslint-loader 会将 lint 错误输出为编译警告

  // useEslint: false,
  // assetsDir: 'static',

  // css: {
  //   modules: true
  // },

  devServer: {
    // overlay: {
    //   warnings: true,
    //   errors: true
    // }
  },

  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', path.resolve('src'))
      .set('#', path.resolve('static'))
  }
}